/**
 * Created by gparis on 6/12/2017.
 */
import React, {PropTypes} from 'react';
import Main from '../containers/MainLayout.jsx';
import Login from '../containers/Login.jsx';

const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        component: Main,
        path: '/customer-center'
    },
    {
        component: Main,
        path: '/'
    }
];


export default routes;