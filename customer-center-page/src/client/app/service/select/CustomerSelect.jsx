import React from "react";
import RestService from '../RestService.jsx';

const CustomerSelect = (callback) => {
    let options = [];
    RestService.get('/customers', function(data) {
        $.each(data, function (idx) {
            options.push({value: data[idx].id, label: data[idx].contact.companyName});
        });
        callback(options);
    })
};

export default CustomerSelect;