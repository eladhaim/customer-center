import React from "react";
import RestService from '../RestService.jsx';

const DistributorSelect = (callback) => {
    let options = [];
    RestService.get('/distributors', function(data) {
        $.each(data, function (idx) {
            options.push({value: data[idx].id, label: data[idx].partner});
        });
        callback(options);
    })
};

export default DistributorSelect;