import React from 'react';
import RestService from '../RestService.jsx';

const RenewalTypeSelect = (callback) => {
    let options = [];
    RestService.get('/renewals', function(data) {
        $.each(data, function (idx) {
            options.push({value: idx, label: data[idx]});
        });
        callback(options);
    })
};

export default RenewalTypeSelect;