import React from 'react';
import customers from './CustomerSelect.jsx';
import distributors from './DistributorSelect.jsx';
import renewals from './RenewalTypeSelect.jsx';
import countries from './CountrySelect.jsx';

const SelectDispatcher = (name, callback) => {
    switch(name) {
        case 'renewal':
            return renewals(callback);
        case 'customer':
            return customers(callback);
        case 'distributor':
            return distributors(callback);
        case 'country':
            return countries(callback);
    }
};

export default SelectDispatcher;