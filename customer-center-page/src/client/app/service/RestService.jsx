/**
 * Created by gparis on 6/19/2017.
 */

const RestService = (function() {
    //noinspection JSUnresolvedVariable
    let url = _API;
    return {
        get: function(endpoint, callback) {
            $.ajax({
                type: 'GET',
                url: url + endpoint,
                success: function(data){
                    callback(data)
                }
            });
        },
        post: function(endpoint, successCallback, errorCallback, data) {
            $.ajax({
                type: 'POST',
                data: data,
                contentType: 'application/json',
                url: url + endpoint,
                success: function(data){
                    successCallback(data)
                },
                error: function (data) {
                    errorCallback(data);
                }
            });
        },
        put: function(endpoint, successCallback, errorCallback, data) {
            $.ajax({
                type: 'PUT',
                data: data,
                url: url + endpoint,
                contentType: 'application/json',
                success: function(data){
                    successCallback(data)
                },
                error: function (data) {
                    errorCallback(data);
                }
            });
        },
        patch: function(endpoint, successCallback, errorCallback, data) {
            $.ajax({
                type: 'PATCH',
                data: data,
                url: url + endpoint,
                contentType: 'application/json',
                success: function(data){
                    successCallback(data)
                },
                error: function (data) {
                    errorCallback(data);
                }
            });
        },
        delete: function(endpoint, successCallback, errorCallback) {
            $.ajax({
                type: 'DELETE',
                url: url + endpoint,
                contentType: 'application/json',
                success: function(data){
                    successCallback(data)
                },
                error: function (data) {
                    errorCallback(data);
                }
            });
        }
    }
})();

export default RestService;