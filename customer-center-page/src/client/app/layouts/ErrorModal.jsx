import React from 'react';
import Dialog from 'material-ui/Dialog';

class ErrorModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    toggleModal() {
        this.setState({open: !this.state.open});
    }

    setMessage(content) {
        this.setState({content: content});
        this.toggleModal();
    }

    render() {
        const actions = [
            <a
                className="waves-effect waves-grey btn-flat"
                onClick={()=>this.toggleModal()}
            >OK</a>
        ];

        return (
            <div id="errorModal" name="errorModal">
                <Dialog
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                >
                    {this.state.content ?
                        <div>
                            {this.state.content.map((c,k)=>{return k == 0 ? <h5 key={k} className="light">{c}</h5> : <h6 key={k}>{c}</h6>})}
                        </div>
                    : null}
                </Dialog>
            </div>
        );
    }
}

export default ErrorModal;