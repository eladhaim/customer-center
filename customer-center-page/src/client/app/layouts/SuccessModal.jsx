import React from 'react';
import Dialog from 'material-ui/Dialog';

class SuccessModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    toggleModal() {
        this.setState({open: !this.state.open});
    }

    setSuccessContent(content) {
        this.setState({content: content});
        this.toggleModal();
    }

    render() {
        const actions = [
            <a
                className="waves-effect waves-grey btn-flat"
                onClick={()=>this.toggleModal()}
            >OK</a>
        ];

        return (
            <div id="successModal" name="successModal">
                <Dialog
                    actions={actions}
                    modal={true}
                    open={this.state.open}
                    contentClassName="success-modal"
                >
                    <h5 className="light">{this.props.message}</h5>
                    {this.state.content ?
                        <table>
                            {this.state.content.map((c,k)=>{return <tr key={k}>
                                <th className="korm-content">{c.key}</th>
                                <td className="korm-content">{c.value}</td>
                            </tr>})}
                        </table>
                         : null}
                </Dialog>
            </div>
        );
    }
}

export default SuccessModal;