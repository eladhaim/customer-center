import React from "react";
import ReactDOM from "react-dom";
import {Router, browserHistory} from "react-router";
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import routes from './constants/routes.jsx';

injectTapEventPlugin();

ReactDOM.render(
    <MuiThemeProvider muiTheme={getMuiTheme()}>
        <Router
            history={browserHistory}
            routes={routes}/>
    </MuiThemeProvider>, document.getElementById('app'));