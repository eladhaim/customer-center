import React from 'react';
import Header from '../components/commons/Header.jsx'
import Footer from '../components/commons/Footer.jsx'

class MainLayout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div id="main-wrapper">
                <Header/>
                <main>
                    {this.props.children}
                </main>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default MainLayout;

