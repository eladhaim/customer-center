import React from 'react';
import {Link} from 'react-router';
import Footer from '../components/commons/Footer.jsx';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {valid: false};
    }

    componentDidMount() {
        $("#loginForm").validate({
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });
        $("#loginButton").click( function(){
            if( $("#loginForm").valid() ){
                this.setState({valid: !this.state.valid});
            }
        }.bind(this));
    }

    render() {
        return (
            <div>
                <div className="main-wrapper login-screen">
                    <main>
                        <div className="center login-screen">
                            <div className="row">
                                <a href="#!" className="brand-logo centered">
                                    <img src="assets/img/adistec.png" />
                                </a>
                            </div>
                            <div className="row login-form-conainer card">
                                <form id="loginForm" className="col s12" method="post">
                                    <div className='row'>
                                        <div className='input-field col s12'>
                                            <input className='validate' type='email' name='email' id='email' required='required' />
                                            <label htmlFor='email'>Enter your email</label>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='input-field col s12'>
                                            <input className='validate' type='password' name='password' id='password' required='required' />
                                            <label htmlFor='password'>Enter your password</label>
                                        </div>
                                    </div>
                                    <br />
                                    <div className='row'>
                                        {/*<button id="loginButton" type="button" classNam  e='col s12 btn waves-effect'>Login</button>*/}
                                        <Link id="loginButton" to={this.state.valid ? "korm/subscriptions" : "/login"} className='col s12 btn waves-effect'>Login</Link>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </main>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Login;