/**
 * Created by gparis on 6/15/2017.
 */
import React, {PropTypes} from 'react';
import Validate from './ValidateForm.js';

const TextField = ({
    name,
    type,
    title,
    col,
    validate,
    error,
    handler,
    onClick
}) => {
    let className = "input-field col s" + col;
    return (
        <div className={className} id={name}>
            <input type={type ? type : 'text'}
                   className={validate ? 'validate' : ''}
                   name={name}
                   pattern={validate ? Validate.pattern(validate) : ''}
                   data-required={validate ? 'required' : 'optional'}
                   onChange={handler}
                   onClick={onClick}
            />
            <label htmlFor={name}
                   data-error={error}
            >{title}</label>
        </div>
    )
};

TextField.PropTypes = {
    col: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string,
    validate: PropTypes.string,
    error: PropTypes.string,
    pattern: PropTypes.string,
    handler: PropTypes.func.isRequired,
    onClick: PropTypes.func
};

export default TextField;