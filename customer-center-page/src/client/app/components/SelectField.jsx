/**
 * Created by gparis on 6/15/2017.
 */
import React, {PropTypes} from 'react';

const SelectField = ({

}) => {

};

SelectField.PropTypes = {
    col: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    select: PropTypes.string.isRequired,
    error: PropTypes.string,
    validate: PropTypes.string,
    defaultMessage: PropTypes.string
};

export default SelectField;