/**
 * Created by gparis on 7/17/2017.
 */
import React from 'react';
import {Link} from 'react-router';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';

const Button = styled(RaisedButton)`
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12) !important;
    width: 100%;
    margin-bottom: inherit;
    margin-top: inherit;
    vertical-align: middle !important;
    z-index: 1 !important;
    will-change: opacity, transform !important;
    transition: all .3s ease-out !important;
    
    button {
        background-color: #005fab !important;
        
        &:hover {
            background-color: #008fcb !important;
        }
        
        div {
            div { 
                span {
                    color: #FAFAFA !important;
                }
            }
        }
    }
    
    @media screen and (max-width: 1268px) {
        span {
            font-size: 12px !important;
            padding: 0 !important;
        }
        
        i {
            margin-right: 8px !important;
            margin-left: 0 !important;
        }
    }
`;

const AdistecButton = ({
    label,
    onClick,
    to,
    width,
    marginBottomTop,
    marginRight,
    disabled,
    className,
    icon,
    type
}) => (
    <div style={
        {
            marginTop: `${marginBottomTop ? marginBottomTop : '0'}px`,
            marginBottom: `${marginBottomTop ? marginBottomTop : '0'}px`,
            marginRight: `${marginRight ? marginRight : '0'}px`,
            width: `${width ? width : '100'}%`,
            float: `${className ? className : 'initial'}`
        }
    }>
        <Link to={to}>
            <Button onTouchTap={onClick} label={label} disabled={disabled} className={className} icon={icon} type={type}/>
        </Link>
    </div>
);

export default AdistecButton;