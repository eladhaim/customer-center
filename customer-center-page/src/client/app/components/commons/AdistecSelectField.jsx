/**
 * Created by gparis on 7/20/2017.
 */
import React from 'react';
import styled from 'styled-components';
import MenuItem from 'material-ui/MenuItem';
import {SelectField} from 'redux-form-material-ui';
import {Field} from "redux-form";

const StyledSelect = styled(SelectField)`
    width: 100% !important;
    box-sizing: border-box !important;    
    height: 61px !important;
    
    /* Select floating label */
    label {
        top: 45% !important;
    }
    
    /* Select content */
    div > div {
        bottom: 10px !important;
        margin-top: 0 !important;
    }
    
    /* Select arrow */
    button {
        bottom: 10px !important;
    }
    
    /* Select error field */
    div:nth-child(4) {
        bottom: 18px !important;
    }
`;

const AdistecSelect = ({
    name,
    label,
    items,
    size,
    validate
}) => (
    <div className={size ? 'col s' + size : ''} style={{height: '71px'}}>
        <Field floatingLabelText={label}
               component={StyledSelect}
               menuItemStyle={{height: '20%'}}
               validate={validate}
               name={name}>
            {items ? items.map((option, k)=>{return <MenuItem key={k} value={option.value} primaryText={option.label}/>}) : null}
        </Field>
    </div>
);

export default AdistecSelect;