/**
 * Created by gparis on 7/14/2017.
 */
import React from 'react';
import styled from 'styled-components';
import {TextField} from "redux-form-material-ui";
import {Field} from "redux-form";

const StyledTextField = styled(TextField)`
    width: 100% !important;
    box-sizing: border-box !important;    
    height: 61px !important;
    margin-bottom: 10px !important;
    
    /* Text floating label */
    label {
        top: 45% !important;
    }
    
    /* Text input */
    input {
        margin-top: 22px !important;
        height: 50% !important;
    }
    
    /* Text error field */
    div:nth-child(4) {
        top: 5px !important;
        float: left !important;
    }
`;

const AdistecTextField = ({
    type,
    name,
    label,
    className,
    size,
    validate
}) => (
    <div className={size ? 'col s' + size : ''}>
        <Field  name={name}
                component={StyledTextField}
                floatingLabelText={label}
                className={className}
                type={type}
                style={{width: "100%"}}
                validate={validate}/>
    </div>
);

export default AdistecTextField;