import React from "react";
import FlatButton from 'material-ui/FlatButton';
import AccountMenu from '../AccountMenu.jsx';
import styled from 'styled-components';

const HeaderMain = styled.header `
    padding: 14px 0px 15px 0px;
    z-index: 9;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
`;

const HeaderContainer = styled.div `
    margin: 0 auto;
    max-width: 1280px;
    width: 90%;
    
    @media only screen and (min-width: 601px){
        width: 85%;
    }
    
    @media only screen and (min-width: 993px){
        width: 70%;
    }
`;

const AccountInfo = styled.div `
    float: right !important;
`;

const LogoStyle = styled.div `
    float: left !important;
    margin-top: 5px;
    margin-right: 20px;
    
    a {
        width: 100%;
        text-align: center;
        padding-top: 6px;
        
        img {
          height: 30px;
          content: url("assets/img/adistec-cc-logo.png");
        }
    }
`;

const AppNameStyle = styled.div `
    float: left !important;
    margin-top: 5px;
    color: white;
`;

const AccountHeader = styled(FlatButton)`
    color: #FAFAFA !important;
    top: 3px !important;
    div > span {
        font-size: 17px !important; 
        text-transform: none !important; 
        font-weight: bold;
    }
`;

const Logo = () => <LogoStyle><a><img/></a></LogoStyle>;

const AppName = ({appName}) => <AppNameStyle><h3>{appName}</h3></AppNameStyle>

const Header = ({
    onLogOut,
    onClick,
    open,
    auth,
    username,
    useremail,
    appName
}) => (
    <HeaderMain>
        <HeaderContainer>
            <Logo/>
            {appName ? <AppName appName="appName"/> : null}
            {auth ?
                <div>
                    <AccountInfo>
                        <AccountHeader label={username}
                                       id="account-header"
                                       labelPosition="before"
                                       icon={<i className="material-icons">account_circle</i>}
                                       onClick={onClick}/>
                        <AccountMenu useremail={useremail}
                                     onLogOut={onLogOut}
                                     open={open}
                                     element={document.getElementById("account-header")}
                                     onClose={onClick}/>
                    </AccountInfo>
                </div> : null}
        </HeaderContainer>
    </HeaderMain>
);

export default Header;