import React from "react";

const Footer = () => (
    <footer>
        <p>Copyright © 2017 Adistec Corp. All rights reserved.</p>
    </footer>
);

export default Footer;