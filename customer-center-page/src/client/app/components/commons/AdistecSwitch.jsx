/**
 * Created by gparis on 7/20/2017.
 */
import React from 'react';
import {Toggle} from 'redux-form-material-ui';
import {Field} from "redux-form";

const AdistecSwitch = ({
    name,
    title,
    defaultChecked,
    uncheckedText,
    checkedText,
    titlePosition,
    size
}) => (
    <div className={size ? 'col s' + size : ''}>
        <Field  defaultToggled={defaultChecked}
                label={title}
                component={Toggle}
                name={name}
                labelPosition={titlePosition}/>
    </div>
);

export default AdistecSwitch;