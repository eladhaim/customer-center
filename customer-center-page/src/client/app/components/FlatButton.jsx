/**
 * Created by gparis on 6/16/2017.
 */
import React, {PropTypes} from 'react';

const styles = {
    container: {
        border: '10px',
        boxSizing: 'border-box',
        display: 'inline-block',
        fontFamily: 'Roboto, sans-serif',
        WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
        cursor: 'pointer',
        textDecoration: 'none',
        margin: '4px 8px 8px 0px',
        padding: '0px',
        outline: 'none',
        fontSize: 'inherit',
        fontWeight: 'inherit',
        position: 'relative',
        zIndex: '1',
        height: '36px',
        lineHeight: '36px',
        minWidth: '64px',
        color: 'rgb(0, 95, 171)',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        borderRadius: '2px',
        userSelect: 'none',
        overflow: 'hidden',
        backgroundColor: 'rgba(0, 0, 0, 0)',
        textAlign: 'center',
        maxHeight: '36px'
    },
    a: {
        position: 'relative',
        paddingLeft: '16px',
        paddingRight: '16px',
        verticalAlign: 'middle',
        letterSpacing: '0px',
        textTransform: 'uppercase',
        fontWeight: '500',
        fontSize: '14px'
    }
};

const FlatButton = ({
    title,
    handler
}) => {
    return (
        <a type="button" style={styles.container} onClick={handler}>
            <div>
                <span style={styles.a}>{title}</span>
            </div>
        </a>
    );
};

FlatButton.PropTypes = {
    title: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired
};

export default FlatButton;