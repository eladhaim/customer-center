/**
 * Created by gparis on 6/14/2017.
 */
import React, {PropTypes} from 'react';
import DatePicker from 'material-ui/DatePicker';

class DateInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {col: props.col, name: props.name, title: props.title, active: false};
    }

    componentDidMount = () => {
        let input = $('#' + this.state.name);
        input.next().remove();
        input.css('color', 'inherit');
        input.css('border', '');
    };

    activeLabel = (e, date) => {
        let month = (date.getMonth() < 10 ? "0" : "") + (+date.getMonth() + 1);
        let expirationDate = date.getFullYear() + "-" + month + "-01";

        let data = {target: {name: this.state.name, value: expirationDate}};

        this.props.handler(data);
        this.setState({active: true});
    };


    render() {
        let textFieldStyle = {width: '100%', bottom: '3px', margin: '0 0 20px 0', border: 'black', color: 'rgba(0,0,0,0.6)'};
        let className = 'input-field col s' + this.state.col;
        return (
            <div className={className}>
                <DatePicker id={this.state.name}
                            name={this.state.name}
                            mode="landscape"
                            textFieldStyle={textFieldStyle}
                            onChange={(e,date)=>this.activeLabel(e,date)}
                />
                <label htmlFor={this.state.name} className={this.state.active ? "active" : "inactive"}>{this.state.title}</label>
            </div>
        );
    }
}

DateInput.PropTypes = {
    col: PropTypes.string.isRequired
};

export default DateInput;