/**
 * Created by gparis on 6/15/2017.
 */
import React, {PropTypes} from 'react';
import FlatButton from '../FlatButton.jsx';
import MonthSelection from './MonthSelection.jsx';
import YearSelection from './YearSelection.jsx';

const styles = {
    container: {
        display:'block',
        color: 'rgba(0, 0, 0, 0.87)',
        userSelect: 'none',
        width: '479px'
    },
    side: {
        width: '135px',
        height: '263px',
        float: 'left',
        fontWeight: '700',
        display: 'inline-block',
        backgroundColor: 'rgb(0, 95, 171)',
        borderTopLeftRadius: '2px',
        borderTopRightRadius: '0px',
        borderBottomLeftRadius: '2px',
        color: 'rgb(255, 255, 255)',
        padding: '20px',
        boxSizing: 'border-box'
    },
    main: {
        display: 'flex',
        flexDirection: 'column'
    },
    mainDiv: {
        display: 'flex',
        alignContent: 'space-between',
        justifyContent: 'space-between',
        flexDirection: 'column',
        fontSize: '12px',
        fontWeight: '400',
        padding: '0px 8px',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
    },
    buttonBar: {position: 'absolute', right: '0px', bottom: '0px'}
};

const MonthPicker = ({

}) => {
    return (
        <div style={styles.container}>
            <div>
                <div style={styles.side}></div>
            </div>
            <div style={styles.main}>
                <div style={styles.mainDiv}>

                    <YearSelection year="2017"/>
                    <MonthSelection month="JUN"/>

                    <div style={styles.buttonBar}>
                        <FlatButton title="Cancel"/>
                        <FlatButton title="OK"/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MonthPicker;