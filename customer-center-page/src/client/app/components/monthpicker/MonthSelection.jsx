/**
 * Created by gparis on 6/16/2017.
 */
import React, {PropTypes} from 'react';
import MonthRow from './MonthRow.jsx';

const styles = {
    monthTable: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        fontWeight: '400',
        height: '228px',
        lineHeight: '2',
        position: 'relative',
        textAlign: 'center'
    }
};

const MonthSelection = (
    currentMonth
) => {
    let month = currentMonth.month;
    return (
        <div style={{position: 'relative',overflow: 'hidden',height: '164px'}}>
            <div style={{position: 'absolute',height: '100%',width: '100%',top: '0px',left: '0px',transition: 'transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, opacity 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'}}>
                <div style={styles.monthTable}>
                    <MonthRow months={['JAN', 'FEB', 'MAR']} month={month}/>
                    <MonthRow months={['APR', 'MAY', 'JUN']} month={month}/>
                    <MonthRow months={['JUL', 'AUG', 'SEP']} month={month}/>
                    <MonthRow months={['OCT', 'NOV', 'DIC']} month={month}/>
                </div>
            </div>
        </div>
    );
};

export default MonthSelection;