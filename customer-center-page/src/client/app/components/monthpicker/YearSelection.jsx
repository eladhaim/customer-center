/**
 * Created by gparis on 6/16/2017.
 */
import React, {PropTypes} from 'react';

const styles = {
    chevronContainer: {
        border: '10px',
        boxSizing: 'border-box',
        display: 'inline-block',
        fontFamily: 'Roboto, sans-serif',
        WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
        cursor: 'pointer',
        textDecoration: 'none',
        margin: '0px',
        padding: '12px',
        outline: 'none',
        fontSize: '0px',
        fontWeight: 'inherit',
        position: 'relative',
        zIndex: '1',
        overflow: 'visible',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: '48px',
        height: '48px',
        background: 'none'
    },
    yearSelectionContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        backgroundColor: 'inherit',
        height: '48px'
    },
    chevronSvg: {
        display: 'inline-block',
        color: 'rgba(0, 0, 0, 0.87)',
        fill: 'currentcolor',
        height: '24px',
        width: '24px',
        userSelect: 'none',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
    },
    yearContainer: {
        position: 'relative',
        overflow: 'hidden',
        height: '100%',
        fontSize: '14px',
        fontWeight: '500',
        textAlign: 'center',
        width: '100%'
    },
    yearInnerContainer: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        top: '0px',
        left: '0px',
        transition: 'transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, opacity 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
    },
    year: {
        height: 'inherit',
        paddingTop: '12px'
    }
};

class YearSelection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {year: props.year}
    }

    backward = (e) => {
        this.setState({year: this.state.year - '1'})
    };

    forward = (e) => {
        this.setState({year: (+this.state.year) + (+'1')})
    };

    render() {
        return (
            <div style={styles.yearSelectionContainer}>
                <button type="button" style={styles.chevronContainer} onClick={this.backward}>
                    <div>
                        <svg viewBox="0 0 24 24" style={styles.chevronSvg}>
                            <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"/>
                        </svg>
                    </div>
                </button>
                <div style={styles.yearContainer}>
                    <div style={styles.yearInnerContainer}>
                        <div style={styles.year}>{this.state.year}</div>
                    </div>
                </div>
                <button type="button" style={styles.chevronContainer} onClick={this.forward}>
                    <div>
                        <svg viewBox="0 0 24 24" style={styles.chevronSvg}>
                            <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"/>
                        </svg>
                    </div>
                </button>
            </div>
        );
    }
}

export default YearSelection;