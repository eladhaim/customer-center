/**
 * Created by gparis on 6/16/2017.
 */
import React, {PropTypes} from 'react';

const styles = {
    monthRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: '34px',
        marginBottom: '2px'
    },
    month: {
        border: '10px',
        boxSizing: 'border-box',
        display: 'inline-block',
        fontFamily: 'Roboto, sans-serif',
        WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
        cursor: 'pointer',
        textDecoration: 'none',
        margin: '0px',
        padding: '0px 0px',
        outline: 'none',
        fontSize: '18',
        fontWeight: '600',
        position: 'relative',
        zIndex: '1',
        width: '54px',
        background: 'none'
    },
    monthDiv: {
        backgroundColor: 'rgb(0, 95, 171)',
        borderRadius: '15%',
        height: '100%',
        opacity: '0',
        position: 'absolute',
        transform: 'scale(0)',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: '100%'
    },
    monthSpan: {
        color: 'rgba(0, 0, 0, 0.87)',
        fontWeight: '400',
        position: 'relative'
    },
    monthDivSelected: {
        backgroundColor: 'rgb(0, 95, 171)',
        borderRadius: '15%',
        height: '100%',
        opacity: '1',
        position: 'absolute',
        transform: 'scale(1)',
        transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        width: '100%'
    },
    monthSpanSelected: {
        color: 'rgba(255, 255, 255, 1)',
        fontWeight: '400',
        position: 'relative'
    }
};

class MonthRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {months: props.months, selected: props.month};
    }

    over = (e) => {
        let div = e.currentTarget.children;
        if(div[1].textContent != this.state.selected) {
            div[0].style.transform = 'scale(1)';
            div[0].style.opacity = '0.6';
            div[1].style.color = 'rgba(255, 255, 255, 0.87)'
        }
    };

    out = (e) => {
        let div = e.currentTarget.children;
        if(div[1].textContent != this.state.selected) {
            div[0].style.transform = 'scale(0)';
            div[0].style.opacity = '0';
            div[1].style.color = 'rgba(0, 0, 0, 0.87)'
        }
    };

    click = (e) => {
        let month = e.currentTarget.textContent;
        this.setState({selected: month});
    };

    render() {
        return (
            <div style={styles.monthRow}>
                {this.state.months.map((m,k)=>{console.log(m); return  <div onMouseOver={this.over} onMouseOut={this.out} key={k} type="button" style={styles.month}>
                    <div style={this.state.selected == m ? styles.monthDivSelected : styles.monthDiv}></div>
                    <a style={this.state.selected == m ? styles.monthSpanSelected : styles.monthSpan}
                       onClick={this.click}>{m}</a>
                </div>})}
            </div>
        )
    }

}

export default MonthRow;