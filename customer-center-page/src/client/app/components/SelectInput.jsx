import React, {PropTypes} from 'react';
import Validate from './ValidateForm.js';
import SelectDispatcher from '../service/select/SelectDispatcher.jsx';

class SelectInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {id: props.id, title: props.title, col: props.col,
            validate: props.validate, error: props.error, name: props.name,
            select: props.select, data: [], defaultMessage: props.defaultMessage
        };
    }

    componentWillMount = () => {
        SelectDispatcher(this.state.select, this.loadComponentOptions);
    };

    componentDidMount = () => {
        this.mountMaterialComponent();
    };

    loadComponentOptions = (data) => {
        this.setState({data: data});
        this.mountMaterialComponent();
    };

    mountMaterialComponent = () => {
        let select = $('#' + this.state.id);
        select.on('change', function (e) {
            let input = select.parent().find('input');
            input.removeClass('invalid').addClass('valid');
            this.props.handler(e);
        }.bind(this)).material_select();

        let input = select.parent().find('input');
        if(this.state.validate) {
            let pattern = Validate.pattern(this.state.validate);
            input.attr('required', '');
            input.attr('pattern', pattern);
        }

        if(!input.next().is('label'))
            $('<label style="left:auto;" class="active"></label>').text(this.state.title).attr('for', this.state.id).attr('data-error', this.state.error).insertAfter(input);
    };

    render() {
        let className = "input-field col s" + this.state.col;
        return (
            <div className={className}>
                <select id={this.state.id} defaultValue="" name={this.state.name}>
                    <option value="" disabled>{this.state.defaultMessage}</option>
                    {this.state.data.map((opt,k) => {return <option key={k} label={opt.label} value={opt.value}>{opt.label}</option>})}
                </select>
            </div>
        );
    };
}

SelectInput.PropTypes = {
    col: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    select: PropTypes.string.isRequired,
    error: PropTypes.string,
    validate: PropTypes.string,
    defaultMessage: PropTypes.string
};

export default SelectInput;