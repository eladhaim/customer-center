/**
 * Created by gparis on 6/7/2017.
 */

class ValidateForm {
    static validate(formName) {
        let form = $('#' + formName);
        let valid = true;
        form.find('input').each(function() {
                //noinspection JSUnresolvedFunction
                let self = $(this);
                if(self.prop('required')) {
                    if((self.hasClass('select-dropdown'))) {
                        if (self.parent().find('ul > li.active, li.active.selected').length == 0) {
                            self.addClass('invalid');
                            valid = false;
                        }
                    }
                    if(self.hasClass('invalid'))
                        valid = false;
                    if(!self.hasClass('invalid') && (self.val().length == 0)) {
                        self.addClass('invalid');
                        self.parent().find('label').addClass('active');
                        valid = false;
                    }
                }
                if(self.attr('data-required') === 'required') {
                    if((self.hasClass('select-dropdown'))) {
                        if (self.parent().find('ul > li.active, li.active.selected').length == 0) {
                            self.addClass('invalid');
                            valid = false;
                        }
                    }
                    if(self.hasClass('invalid'))
                        valid = false;
                    if(!self.hasClass('invalid') && (self.val().length == 0)) {
                        self.addClass('invalid');
                        self.parent().find('label').addClass('active');
                        valid = false;
                    }
                }
            }
        );
        return valid;
    }

    static pattern(type) {
        let pattern;
        switch (type) {
            case 'text':
                pattern = '^.+$';
                break;
            case 'number':
                pattern = '[0-9]{1,}';
                break;
            case 'country':
                pattern = '[A-Z]{3,}';
                break;
            default:
                break;
        }

        return pattern;
    }
}

export default ValidateForm;