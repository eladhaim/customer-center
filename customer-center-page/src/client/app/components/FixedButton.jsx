import React from 'react';

const FixedButton = ({
    action
}) => (
    <div className="fixed-action-btn korm-fixed-button right">
        <a className="right btn-floating waves-effect waves-light btn-large blue" onClick={action}>
            <i title="Create New" className="material-icons large">add</i>
        </a>
    </div>
);

export default FixedButton;