/**
 * Created by gparis on 8/17/2017.
 */
import React from "react";
import MenuItem from "material-ui/MenuItem";
import Popover from "material-ui/Popover";
import Menu from "material-ui/Menu";
import styled from "styled-components";
import Divider from 'material-ui/Divider';

const AccountDropDown = styled(Popover)`
    top: 60px !important;
`;

const Arrow = styled.div `
    width: 0;
    height: 0;
    top: 3px;
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
    border-bottom: 8px solid rgb(255, 255, 255);
    position: relative;
    left: 82%;
    opacity: ${props=>props.open ? 1 : 0};
    transition: transform 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms, opacity 250ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
    z-index: 5000;
`;

const AccountMenu = ({
    onLogOut,
    onClose,
    open,
    element,
    useremail
}) => (
    <Arrow open={open}>
        <AccountDropDown open={open}
                         anchorEl={element}
                         anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
                         targetOrigin={{horizontal: 'right', vertical: 'top'}}
                         onRequestClose={onClose}>
            <Menu>
                <MenuItem leftIcon={<i style={{color: '#005fab'}} className="material-icons">email</i>} primaryText={useremail}/>
                <Divider />
                <MenuItem onTouchTap={onLogOut} leftIcon={<i style={{color: '#005fab'}} className="material-icons">power_settings_new</i>} primaryText="Log out"/>
            </Menu>
        </AccountDropDown>
    </Arrow>
);

export default AccountMenu;