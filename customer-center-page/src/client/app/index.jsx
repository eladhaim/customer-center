import React from "react";
import ReactDOM from "react-dom";
import {Router, hashHistory} from "react-router";
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import routes from './constants/routes.jsx';

injectTapEventPlugin();
const muiTheme = getMuiTheme({
    datePicker: {
        selectColor: '#005fab',
        color: '#005fab'
    },
    flatButton: {
        primaryTextColor: '#005fab'
    }
});

ReactDOM.render(
    <MuiThemeProvider muiTheme={muiTheme}>
        <Router
            history={hashHistory}
            routes={routes}/>
    </MuiThemeProvider>, document.getElementById('app'));