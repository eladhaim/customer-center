/**
 * Created by gparis on 6/12/2017.
 */
$(document).ready(function() {
    $('select').material_select();
    $('.slider').slider();
    $('.parallax').parallax();
    $('.scrollspy').scrollSpy();
    $(".dropdown-button").dropdown();
    $(".button-collapse").sideNav();
    $('.carousel').carousel();
    Materialize.updateTextFields();
});
