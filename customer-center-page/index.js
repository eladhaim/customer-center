const express = require('express');

const app = express();
app.use(express.static('./src/server/static'));
app.use(express.static('./src/client/bundle'));

app.listen(3000, () => {
    console.log('Server is running on http://localhost:3000')
});