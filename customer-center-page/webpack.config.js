let webpack = require('webpack');
let path = require('path');

let BUILD_DIR = path.resolve(__dirname, 'src/server/static/js');
let APP_DIR = path.resolve(__dirname, 'src/client/app');
let SERVER_DIR = path.resolve(__dirname, 'src/server/static');

let apiHost;
let index;
let setupAPI = function () {
    switch (process.env.NODE_ENV) {
        case 'dev':
            apiHost = "'http://localhost:8080/customer-center-endpoint'";
            index = '/index.jsx';
            break;
        case 'prod':
        default:
            apiHost = "'http://67.94.57.11:8080/ustomer-center-endpoint'";
            index = '/index.prod.jsx';
            break;
    }
};

setupAPI();

let config = {
    entry: APP_DIR + index,
    output: {
        path: BUILD_DIR,
        filename: 'app.js',
        publicPath: '/js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "react", "stage-0"]
                }
            }
        ]
    },
    plugins: [
        /*new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false
            }
        }),*/
        new webpack.DefinePlugin({
            _API: apiHost
        })
    ],
    devServer: {
        historyApiFallback: true,
        port: 3000,
        contentBase: SERVER_DIR
    },
    resolve: {
        extensions: ['.js','.jsx']
    },

    watch: true
};

module.exports = config;