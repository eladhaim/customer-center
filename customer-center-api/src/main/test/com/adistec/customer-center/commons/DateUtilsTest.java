package com.adistec.customercenter.commons;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by gparis on 5/12/2017.
 */
public class DateUtilsTest {
    private long EPOCH;
    private String MS_DATE;
    private String DATE;

    @Before
    public void setUp() {
        EPOCH = 1494547200000L;
        MS_DATE = "/Date(1494547200000+0)/";
        DATE = "2017-05-12";
    }

    @Test
    public void epochToMSDateTimeTest() throws Exception {
        final String msDate = DateUtils.epochToMSDateTime(EPOCH);
        assertEquals(MS_DATE, msDate);
    }

    @Test
    public void msDateTimeToEpochTest() throws Exception {
        final long epoch = DateUtils.msDateTimeToEpoch(MS_DATE);
        assertEquals(EPOCH, epoch);
    }

    @Test
    public void epochToDateTest() throws Exception {
        final String date = DateUtils.epochToDate(EPOCH);
        assertEquals(DATE, date);
    }

    @Test
    public void dateToMSDateTimeTest() throws Exception {
        final String msDate = DateUtils.dateToMSDateTime(DATE);
        assertEquals(MS_DATE, msDate);
    }

}