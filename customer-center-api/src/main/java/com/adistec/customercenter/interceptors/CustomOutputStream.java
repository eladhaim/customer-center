package com.adistec.customercenter.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintStream;

/**
 * Created by gparis on 4/12/2017.
 */
public class CustomOutputStream extends PrintStream {
    private static final Logger log = LoggerFactory.getLogger(CustomOutputStream.class);

    public CustomOutputStream(PrintStream out) {
        super(out);
    }

    @Override
    public void println(String line) {
        if(!line.contains("chain [") && !line.contains("Found trusted certificate:"))
            log.info(line);
    }
}
