package com.adistec.customercenter.service;

import com.adistec.customercenter.dto.OpportunityDTO;
import com.adistec.customercenter.dto.QuoteDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerCenterService {
    public List<QuoteDTO> getQuotes(){
        List<QuoteDTO> quotes = new ArrayList<>();
        quotes.add(new QuoteDTO() {{
            setId(1);
            setActive(true);
        }});

        quotes.add(new QuoteDTO() {{
            setId(2);
            setActive(false);
        }});

        return quotes;
    }

    public List<OpportunityDTO> getOpportunities(){
        List<QuoteDTO> quotes = new ArrayList<>();
        List<OpportunityDTO> opportunities = new ArrayList<>();
        quotes.add(new QuoteDTO() {{
            setId(1);
            setActive(true);
        }});

        quotes.add(new QuoteDTO() {{
            setId(2);
            setActive(false);
        }});

        opportunities.add(new OpportunityDTO(){{
            setId(1);
            getQuotes().addAll(quotes);
        }});
        quotes.clear();

        quotes.add(new QuoteDTO() {{
            setId(3);
            setActive(true);
        }});

        quotes.add(new QuoteDTO() {{
            setId(4);
            setActive(false);
        }});

        opportunities.add(new OpportunityDTO(){{
            setId(2);
            getQuotes().addAll(quotes);
        }});

        return opportunities;
    }
}
