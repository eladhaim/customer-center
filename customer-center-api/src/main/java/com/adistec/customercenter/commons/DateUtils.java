package com.adistec.customercenter.commons;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by gparis on 4/19/2017.
 */
public class DateUtils {

    final static TimeZone TIMEZONE = TimeZone.getTimeZone("GMT");

    public static String epochToMSDateTime(final long epoch) {
        return new StringBuilder()
                .append("/Date(")
                .append(epoch)
                .append("+0)/")
                .toString();
    }

    public static long msDateTimeToEpoch(final String msDateTime) {
        return Long.valueOf(msDateTime.replaceAll("[/Date()]", "").replaceAll("\\+[0-9)/]*", ""));
    }

    public static String epochToDate(final long epoch) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TIMEZONE);

        return sdf.format(new Date(epoch));
    }

    public static String dateToMSDateTime(final String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TIMEZONE);

        Date dateTime = null;
        try {
            dateTime = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return epochToMSDateTime(dateTime != null ? dateTime.getTime() : 0);
    }
}
