package com.adistec.customercenter.dto;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class OpportunityDTO {

    private long id;
    private List<QuoteDTO> quotes = new ArrayList<QuoteDTO>();

    public void setId(long id) {
        this.id = id;
    }

    public List<QuoteDTO> getQuotes() {
        return quotes;
    }

    public long getId() {
        return id;
    }
}
