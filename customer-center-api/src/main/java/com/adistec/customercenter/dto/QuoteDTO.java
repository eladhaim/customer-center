package com.adistec.customercenter.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class QuoteDTO {
    private long id;
    private boolean active;

    public void setId(long id) {
        this.id = id;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public boolean isActive() {
        return active;
    }
}
