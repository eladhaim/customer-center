package com.adistec.customercenter.controller;

import com.adistec.customercenter.dto.QuoteDTO;
import com.adistec.customercenter.service.CustomerCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gparis on 4/27/2017.
 */

@RestController
@RequestMapping(value = "/quote")
public class QuoteController {
    @Autowired
    private CustomerCenterService customerCenterService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ResponseEntity<?> getAllQuotes() {
        List<QuoteDTO> quotes = customerCenterService.getQuotes();
        return new ResponseEntity<>(quotes, HttpStatus.OK);
    }
}
