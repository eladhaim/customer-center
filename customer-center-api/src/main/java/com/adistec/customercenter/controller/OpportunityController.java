package com.adistec.customercenter.controller;

import com.adistec.customercenter.dto.OpportunityDTO;
import com.adistec.customercenter.service.CustomerCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by gparis on 4/27/2017.
 */

@RestController
@RequestMapping(value = "/opportunity")
public class OpportunityController {

    @Autowired
    private CustomerCenterService customerCenterService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ResponseEntity<?> getAllOpportunities() {
        List<OpportunityDTO> opportunities = customerCenterService.getOpportunities();
        return new ResponseEntity<>(opportunities, HttpStatus.OK);
    }
}
